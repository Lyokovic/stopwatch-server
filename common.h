/*
 * Common constants
 */

#define DEFAULT_PORT 8888

#define START_CMD "start"
#define STOP_CMD "stop"

#define CMD_MAXLEN 32
#define ANS_MAXLEN 32
