#include <cstring>
#include <iostream>
#include <stdexcept>
#include <system_error>

#include <unistd.h>

#include <arpa/inet.h>
#include <sys/socket.h>

#include "client.h"
#include "common.h"

Client::Client(const std::string &addr, int port): _addr(addr), _port(port)
{ }

Client::Client(int port): _addr(DEFAULT_ADDR), _port(port)
{ }

Client::Client(): _addr(DEFAULT_ADDR), _port(DEFAULT_PORT)
{ }

Client::~Client()
{
    if (_state == state::CONNECTED) {
        ::close(_sock);
    }
}

void Client::connect()
{
    if (_state != state::DISCONNECTED) {
        throw std::runtime_error("Cannot connect a non disconnected socket");
    }

    try {
        _do_connect(_addr, _port);
    } catch (std::exception &exn) {
        _sock = -1;
        _state = state::ERROR;

        std::cerr << "Error connecting" << std::endl;

        throw;
    }

    _state = state::CONNECTED;
}

void Client::start()
{
    auto answer = _send_command(START_CMD);

    if (answer.compare(0, 2, "OK") != 0) {
        throw std::runtime_error("Unexpected answer: " + answer);
    }
}

int Client::stop()
{
    auto answer = _send_command(STOP_CMD);

    if (answer.compare(0, 2, "OK") != 0) {
        throw std::runtime_error("Unexpected answer: " + answer);
    }

    return std::stoi(answer.substr(4));
}

std::string Client::_send_command(const std::string &command)
{
    if (::write(_sock, command.c_str(), sizeof(command.c_str())) < 0) {
        throw std::system_error(errno, std::generic_category(), "Cannot write");
    }

    char read_buf[ANS_MAXLEN] = {'\0'};
    int read_bytes = ::read(_sock, &read_buf, ANS_MAXLEN - 1);

    if (read_bytes < 0) {
        throw std::system_error(errno, std::generic_category(), "Cannot read answer");
    } else if (read_bytes == 0) {
        ::close(_sock);
        _sock = -1;
        _state = state::DISCONNECTED;
        throw std::runtime_error("Server disconnected");
    }

    return std::string(read_buf);
}

void Client::_do_connect(const std::string &addr, int port)
{
    _sock = ::socket(AF_INET , SOCK_STREAM , 0);
    if (_sock == -1) {
        throw std::system_error(errno, std::generic_category(), "Could not create socket");
    }

    struct sockaddr_in options;
    options.sin_family = AF_INET;
    options.sin_addr.s_addr = INADDR_ANY;
    options.sin_port = htons(port);

    if(::inet_pton(AF_INET, addr.c_str(), &options.sin_addr) <= 0) {
        throw std::system_error(errno, std::generic_category(), "Invalid address");
    }

    if (::connect(_sock, (struct sockaddr *) &options, sizeof(options)) < 0) {
        throw std::system_error(errno, std::generic_category(), "Cannot connect to server");
    }
}
