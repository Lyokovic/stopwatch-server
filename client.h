#include <string>

constexpr auto DEFAULT_ADDR = "127.0.0.1";

/*
 * Client class for the stopwatch project.
 *
 * It is a very simple implementation, fully synchronous.
 * To simplify, all error handling has been made through exceptions, using
 * `runtime_error` for logic errors, or protocol error (on our level), and
 * `system_error` for network/system related errors.
 *
 * TODO: define custom exception type
 */
class Client {
    public:
        /*
         * Create a new stopwatch client instance, without connecting to it.
         */
        Client(const std::string &addr, int port);
        Client(int port);
        Client();

        ~Client();

        /*
         * Connect to stopwatch server
         */
        void connect();

        /*
         * Start stopwatch
         */
        void start();

        /*
         * Stop the stopwatch. Time elapsed is returned in milliseconds.
         *
         * TODO: maybe return as std::chrono::milliseconds
         */
        int stop();

    private:
        enum class state {
            DISCONNECTED = 0,
            CONNECTED,
            ERROR,
        };

        void _do_connect(const std::string &addr, int port);
        std::string _send_command(const std::string &command);

        state _state = state::DISCONNECTED;
        int _sock = -1;
        const std::string _addr;
        const int _port;
};
