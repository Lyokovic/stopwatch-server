#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/epoll.h>
#include <sys/socket.h>

#include "common.h"

#define MAX_EVENTS 10

/*
 * Stopwatch project server C implementation.
 *
 * It is very basic for now and supports only one client at a time.
 *
 * Protocol:
 * All commands and answers are one-line strings ended with `\n` (also `\r\n`
 * for commands, to be telnet-compatible).
 * There are two commands:
 *  - start: starts the stopwatch
 *    The answer will be `OK` if it successfully starts, or `KO: <error` in
 *    case of error.
 *    Calling it on a started stopwatch will reset it.
 *  - stop: stops the stopwatch
 *    The answer will be `OK: <time_elapsed>` if it successfully stops, and the
 *    <time_elapsed> is given in milliseconds. The answer will be `KO: <error>`
 *    in case of error.
 * Example:
 * > stop
 * < KO: not started
 * > start
 * < OK
 * > stop
 * < OK: 2300
 */

struct ep_data {
    int fd;
    struct timespec start_time;
};

static int get_chrono_time(const struct timespec *start) {
    struct timespec end;
    if (clock_gettime(CLOCK_MONOTONIC, &end) < 0) {
        perror("Error getting end time\n");
        return -1;
    }

    time_t s_res = end.tv_sec - start->tv_sec;
    long ns_res = end.tv_nsec - start->tv_nsec;
    if (ns_res < 0) {
        s_res--;
        ns_res =  1000000000 + ns_res;
    }

    /* TODO: check milisecond values, weird results */
    return (s_res * 1000) + (((double) ns_res) / 1000000);
}

static int handle_command(struct ep_data *data, const char *cmd, char *answer) {
    //printf("handle command: %s\n", cmd);

    *answer = '\0';

    if (strncmp(cmd, START_CMD, sizeof(START_CMD)) == 0) {
        if (clock_gettime(CLOCK_MONOTONIC, &(data->start_time))) {
            perror("Error getting start time");
            return -1;
        }
        snprintf(answer, ANS_MAXLEN, "OK\n");
    } else if (strcmp(cmd, STOP_CMD) == 0) {
        if (data->start_time.tv_sec == 0) {
            snprintf(answer, ANS_MAXLEN, "KO: not started\n");
            return strlen(answer);
        }
        snprintf(answer, ANS_MAXLEN, "OK: %d\n",
                get_chrono_time(&(data->start_time)));
        data->start_time.tv_sec = 0;
    } else {
        snprintf(answer, ANS_MAXLEN, "KO: unknown command\n");
    }

    return strlen(answer);
}

static void new_client(int ep_fd, int client_sock) {
    struct ep_data *client_data = malloc(sizeof(struct ep_data));

    if (client_data == NULL) {
        printf("Malloc error, not trying to recover\n");
        exit(EXIT_FAILURE);
    }

    client_data->fd = client_sock;
    client_data->start_time.tv_sec = 0;
    client_data->start_time.tv_nsec = 0;

    struct epoll_event ev = {
        .events = EPOLLIN | EPOLLRDHUP,
        .data = {
            .ptr = client_data,
        },
    };

    if (epoll_ctl(ep_fd, EPOLL_CTL_ADD, client_sock, &ev) < 0) {
        perror("Error adding server socket to epoll set");
        return;
    }

    printf("New client connected\n");
}

static void client_data_available(struct ep_data* data) {
    char command[CMD_MAXLEN] = {0};
    int read_size;
    char *end;
    char answer[ANS_MAXLEN] = {0};

    read_size = read(data->fd, command, sizeof(command) - 1);

    end = strchr(command, '\r');
    if (end == NULL) {
        end = strchr(command, '\n');
    }
    if (end == NULL) {
        end = command + CMD_MAXLEN - 1;
    }
    *end = '\0'; // Discard carriage return if any and end command string

    int ret = handle_command(data, command, answer);
    if (ret < 0) {
        printf("Error during command handling\n");
    } else if (ret > 0) {
        if (write(data->fd, answer, strlen(answer)) < 0) {
            perror("Error writing answer to client");
        }
    }

    if (read_size == 0) {
        printf("Unexpected behaviour: nothing to read\n");
    } else if (read_size == -1) {
        perror("Read failed");
    }
}

int main()
{
    int sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1) {
        perror("Could not create socket");
        return 1;
    }

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) {
        perror("setsockopt failed");
        return 1;
    }

    struct sockaddr_in server_params;
    server_params.sin_family = AF_INET;
    server_params.sin_addr.s_addr = INADDR_ANY;
    server_params.sin_port = htons(DEFAULT_PORT);

    if(bind(sock, (struct sockaddr *)&server_params, sizeof(server_params)) < 0) {
        perror("Bind failed.");
        return 1;
    }

    if (listen(sock, 3) < 0) {
        perror("Error listening on socket");
        return 1;
    }

    printf("Waiting for incoming connections...\n");

    int ep_fd = epoll_create1(0);

    struct ep_data srv_data = {
        .fd = sock,
    };
    struct epoll_event srv_evt = {
        .events = EPOLLIN | EPOLLRDHUP,
        .data = {
            .ptr = &srv_data,
        },
    };

    if (epoll_ctl(ep_fd, EPOLL_CTL_ADD, sock, &srv_evt) < 0) {
        perror("Error adding server socket to epoll set");
        return 1;
    }

    struct epoll_event events[MAX_EVENTS];
    int n_events = -1;
    for (;;) {
        n_events = epoll_wait(ep_fd, events, MAX_EVENTS, -1);
        if (n_events < 0) {
            perror("Error during epoll_wait");
            return 1;
        }
        for (int i=0; i < n_events; i++) {
            struct ep_data *data = events[i].data.ptr;
            if ((events[i].events & EPOLLRDHUP)
                    || (events[i].events & EPOLLHUP)) {
                printf("Client disconnected\n");
                if (epoll_ctl(ep_fd, EPOLL_CTL_DEL, data->fd, NULL) < 0) {
                    perror("Error removing socket from epoll set");
                    return 1;
                }
                close(data->fd);
                free(data);
            } else if (events[i].events & EPOLLIN) {
                if (data->fd == sock) {
                    int client_sock = accept(sock, NULL, NULL);
                    if (client_sock < 0) {
                        perror("Client accept failed");
                        continue;
                    }
                    new_client(ep_fd, client_sock);
                } else {
                    client_data_available(data);
                }
            } else {
                printf("Unexpected event on server socket, exiting.\n");
                return 1;
            }
        }
    }

    close(sock);

    return 0;
}
