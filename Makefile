FLAGS := -Wall -Wextra
CFLAGS := $(FLAGS) -std=gnu99
CPPFLAGS := $(FLAGS) -std=c++14

server: server.c
	gcc -o $@ $< $(CFLAGS)

client: libclient.o client_main.cc
	g++ -o $@ $^ $(CPPFLAGS)

libclient.o: client.cc
	g++ -o $@ -c $^ $(CPPFLAGS)
