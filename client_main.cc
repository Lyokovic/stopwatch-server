#include <chrono>
#include <iostream>
#include <thread>

#include "client.h"

int main()
{
    auto client = Client(8888);

    std::cout << "Connecting" << std::endl;
    client.connect();

    std::cout << "Starting" << std::endl;
    client.start();

    std::this_thread::sleep_for(std::chrono::milliseconds(2300));

    std::cout << "Stopping" << std::endl;
    auto time = client.stop();

    std::cout << "Elapsed time: " << time << "ms" << std::endl;
}
